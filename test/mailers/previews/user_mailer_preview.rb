# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def auto_registration_email
    @u = User.first
    @pass = 'xxxxxxxx'
    UserMailer.auto_registration_email(@u, @pass)
  end

  def order_notification
    @order = Order.find(13)
    UserMailer.order_notification(@order)
  end

end
