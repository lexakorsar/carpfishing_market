# encoding: UTF-8
module ApplicationHelper
  def get_class_level level
    case level
      when 1
        li_class = 'margin-10'
      when 2
        li_class = 'margin-20'
      else
        li_class =' '
    end
    li_class
  end

  def calculate_count(count)
    if count.present? && count > 0 && count < 2
      label = '<span title="Мало" class="counts small"></span>'.html_safe
    end
    if count.present? && count > 1
      label = '<span title="Много" class="counts many"></span>'.html_safe
    end
    if !count.present? || count == 0
      label = '<span title="Нет на складе" class="counts none"></span>'.html_safe
    end
    label
  end

  def price_render(product)
    if product.price.present? && product.price.to_s != '0.0'
      product.price
    elsif product.fake_price.present? && product.fake_price.to_s != '0.0' && product.price.present? && product.price.to_s == '0.0'
      product.fake_price
    else
      ' - '
    end
  end
end
