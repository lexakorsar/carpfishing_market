class UserMailer < ActionMailer::Base
  default from: '"Carplux.ru" support@carplux.ru'

  def auto_registration_email(user, password)
    @user = user
    @password = password
    mail(
        to: @user.email,
        :from => 'Carplux.ru <support@carplux.ru>',
        subject: 'Интернет магазин Carplux.ru - регистрация',
        content_type: 'text/html'
    )
  end

  def order_notification(order)
    @order = order

    mail(
        to: @order.user.email,
        :from => 'Carplux.ru <support@carplux.ru>',
        subject: 'Интернет магазин Carplux.ru - Ваш заказ принят в обработку',
        content_type: 'text/html'
    )
  end

  def order_information(order)
    @order = order

    mail(
        to: 'carplux@mail.ru',
        :from => 'Carplux.ru <support@carplux.ru>',
        subject: 'Интернет магазин Carplux.ru - поступил новый заказ',
        content_type: 'text/html'
    )
  end
end
