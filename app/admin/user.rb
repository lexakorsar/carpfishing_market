# encoding: UTF-8
ActiveAdmin.register User do
  menu :parent => 'Пользователи'

  controller do

    def create
      create! do |format|
        format.html { redirect_to admin_users_url}
      end
    end

    def update
      update! do |format|
        format.html { redirect_to admin_users_url}
      end
    end
  end

  index do
    selectable_column
    id_column
    column :fio
    column :discount
    column :email
    column :address
    column :phone
    column :is_guest
    column :current_sign_in_at do |item|
      Russian::strftime(item.created_at, '%d %B %Y')
    end
    column :sign_in_count
    column :created_at do |item|
      Russian::strftime(item.created_at, '%d %B %Y')
    end
    actions
  end

  filter :email
  filter :fio
  filter :address
  filter :phone
  filter :is_guest
  # filter :current_sign_in_at
  # filter :sign_in_count
  # filter :created_at

  form do |f|
    f.inputs 'Данные пользователя' do
      f.input :email
      f.input :fio
      f.input :address
      f.input :phone
      f.input :discount
    end
    f.actions
  end


end
