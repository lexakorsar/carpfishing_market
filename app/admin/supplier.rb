# encoding: UTF-8
ActiveAdmin.register Supplier do

  controller do

    def create
      create! do |format|
        format.html { redirect_to admin_suppliers_url}
      end
    end

    def update
      update! do |format|
        format.html { redirect_to admin_suppliers_url}
      end
    end
  end

  index do
    selectable_column
    id_column
    column :name
    column :type_price

    column :description do |item|
      truncate(item.description, :length => 300, :omission => '...')
    end
    column :created_at do |item|
      Russian::strftime(item.created_at, '%d %B %Y')
    end
    actions
  end

  filter :name
  # filter :current_sign_in_at
  # filter :sign_in_count
  # filter :created_at

  form html: { multipart: true }  do |f|
    f.inputs 'Новый поставщик' do
      f.input :name
      f.input :price
      f.input :type_price
      f.input :description, :as => :ckeditor

    end
    f.actions
  end

end
