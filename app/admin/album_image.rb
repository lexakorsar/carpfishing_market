# encoding: UTF-8
ActiveAdmin.register AlbumImage do

  menu :parent => 'Альбомы'

  index do
    selectable_column
    id_column
    column :image do |item|
      image_tag(item.image.url(:thumb), :height => '100')
    end

    column :name do |i|
      best_in_place i, :name, :type => :input, path: [:admin, i]
    end
    column :album
    column :created_at do |item|
      Russian::strftime(item.created_at, '%d %B %Y')
    end
    actions
  end

end
