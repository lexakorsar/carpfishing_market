# encoding: UTF-8
ActiveAdmin.register News do

  controller do

    def create
      create! do |format|
        format.html { redirect_to admin_news_index_url}
      end
    end

    def update
      update! do |format|
        format.html { redirect_to admin_news_index_url}
      end
    end
  end

  index do
    selectable_column
    id_column
    column :title
    column :title_image do |item|
      image_tag(item.title_image.url(:thumb), :height => '100')
    end
    column :description do |item|
      truncate(item.description, :length => 300, :omission => '...')
    end
    column :created_at do |item|
      Russian::strftime(item.created_at, '%d %B %Y')
    end
    actions
  end

  filter :title
  filter :created_at
  # filter :current_sign_in_at
  # filter :sign_in_count
  # filter :created_at

  form html: { multipart: true }  do |f|
    f.inputs 'Новая новость' do
      f.input :title
      f.input :description, :as => :ckeditor
      f.input :title_image
    end
    f.actions
  end


end
