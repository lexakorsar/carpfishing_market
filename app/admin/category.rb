# encoding: UTF-8
ActiveAdmin.register Category do

  controller do

    def create
      create! do |format|
        format.html { redirect_to admin_categories_url}
      end
    end

    def update
      update! do |format|
        format.html { redirect_to admin_categories_url}
      end
    end
  end

  index do
    selectable_column
    id_column
    column :title

    column :is_show do |i|
      best_in_place i, :is_show, as: :checkbox, url: [:admin, i]
    end

    column :sorting do |i|
      best_in_place i, :sorting, as: :input, url: [:admin, i]
    end
    column :title_image do |item|
      image_tag(item.title_image.url(:thumb), :height => '100')
    end
    column :description do |item|
      truncate(item.description, :length => 300, :omission => '...')
    end
    column :meta_keywords do |i|
      best_in_place i, :meta_keywords, as: :textarea, url: [:admin, i]
    end

    column :meta_description do |i|
      best_in_place i, :meta_description, as: :textarea, url: [:admin, i]
    end

    column :parent
    actions
  end

  filter :title
  filter :parent
  # filter :current_sign_in_at
  # filter :sign_in_count
  # filter :created_at

  form html: { multipart: true }  do |f|
    f.inputs 'Новая категория' do
      f.input :title
      f.input :parent
      f.input :sorting
      f.input :description, :as => :ckeditor
      f.input :title_image
      f.input :meta_keywords
      f.input :meta_description
    end
    f.actions
  end


end
