# encoding: UTF-8
ActiveAdmin.register Admin do
  menu :parent => 'Пользователи'
  permit_params :email, :password, :password_confirmation

  controller do

    def create
      create! do |format|
        format.html { redirect_to admin_admins_url}
      end
    end

    def update
      update! do |format|
        format.html { redirect_to admin_admins_url}
      end
    end
  end

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at do |item|
      Russian::strftime(item.created_at, '%d %B %Y')
    end
    column :sign_in_count
    column :created_at do |item|
      Russian::strftime(item.created_at, '%d %B %Y')
    end
    actions
  end

  filter :email
  filter :created_at
  # filter :current_sign_in_at
  # filter :sign_in_count
  # filter :created_at

  form do |f|
    f.inputs 'Регистрация администратора' do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end
