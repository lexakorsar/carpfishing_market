# encoding: UTF-8
ActiveAdmin.register Product do

  controller do

    def create
      create! do |format|
        format.html { redirect_to admin_products_url}
      end
    end

    def update
      update! do |format|
        format.html { redirect_to admin_products_url}
      end
    end
  end

  scope :all

  scope :carp, :default => true do
    # product.joins(:category).where('categories.title = ?', 'Карпфишинг')
    Category.find_by_title('Карпфишинг').all_products_for_admin
  end

  scope :carp_no_article do
    # product.joins(:category).where('categories.title = ?', 'Карпфишинг')
    Category.find_by_title('Карпфишинг').all_products_for_admin.where(:article => nil)
  end

  scope :carp_no_description do
    # product.joins(:category).where('categories.title = ?', 'Карпфишинг')
    Category.find_by_title('Карпфишинг').all_products_for_admin.where(:description => nil)
  end

  scope :carp_no_count do
    # product.joins(:category).where('categories.title = ?', 'Карпфишинг')
    Category.find_by_title('Карпфишинг').all_products_for_admin.where(:count => 0)
  end

  index do
    selectable_column
    id_column
    column :title_image do |item|

      # if item.rich_file_id.present?
      #   image_tag(Rich::RichFile.find(item.rich_file_id).rich_file.url(:thumb), :height => '100')
      # else
        image_tag(item.title_image.url(:thumb), :height => '100')
      # end
    end
    column :is_active do |i|
      best_in_place i, :is_active, as: :checkbox, url: [:admin, i]
    end

    column :currency do |i|
      best_in_place i, :currency, as: :input, url: [:admin, i]
    end

    # column :is_hit do |i|
    #   best_in_place i, :is_hit, as: :checkbox, url: [:admin, i]
    # end
    #
    # column :is_sale do |i|
    #   best_in_place i, :is_sale, as: :checkbox, url: [:admin, i]
    # end
    column :name do |i|
      best_in_place i, :name, as: :input, url: [:admin, i]
    end
    # column :original_name
    column :count do |i|
      best_in_place i, :count, as: :input, url: [:admin, i]
    end
    column :brand do |i|
      best_in_place i, :brand_id, as: :select, :collection => Brand.select('id, title').to_a.each_with_object({}){ |c,h| h[c.id] = c.title }, url: [:admin, i]
    end
    # column :category
    column :category do |i|
      best_in_place i, :category_id, as: :select, :collection => Category.select('id, title').to_a.each_with_object({}){ |c,h| h[c.id] = c.title }, url: [:admin, i]
    end
    # column :supplier
    # column :article
    # column :product_code
    # column :specifications
    # column :meta_keywords do |i|
    #   best_in_place i, :meta_keywords, :type => :textarea, path: [:admin, i]
    # end

    # column :meta_description do |i|
    #   best_in_place i, :meta_description, :type => :textarea, path: [:admin, i]
    # end

    # column :site_url

    # column :description do |item|
    #   truncate(item.description, :length => 300, :omission => '...')
    # end
    # column :created_at do |item|
    #   Russian::strftime(item.created_at, '%d %B %Y')
    # end
    actions
  end

  filter :name
  filter :price
  filter :article
  filter :product_code
  filter :site_url
  filter :brand
  filter :category
  filter :supplier
  filter :created_at


  form html: { multipart: true } do |f|
    f.inputs 'Продукт' do
      f.input :name
      f.input :title_image, :as => :file, :hint => f.template.image_tag(f.object.title_image.url(:thumb))
      # f.input :rich_file_id, :as => :rich_picker, :config => {  :style => 'width: 400px !important;', :hidden_input => true, :placeholder_image => image_path('placeholder.png'), :preview_size => '200px'}
      f.input :price
      f.input :currency


      f.input :count
      # f.input :specification_products

      # f.nested_fields_for :specifications do |ff|
      #   ff.text_field :name
      # end

      f.has_many :specification_products, :allow_destroy => true, :heading => 'ТТХ', :new_record => true do |s|
         # s.inputs do
        # , :as => :select, :collection => STATUSES
          s.input :specification, :as => :select, :collection => Specification.all.collect {|a| [a.name, a.id]}
          # s.input :units, :as => :select, :collection =>  Unit.all.collect {|b| [b.name, b.name]}
          s.input :value
          # s.specification_products.input :value
         # end
        # if !s.object.nil?
        #   # show the destroy checkbox only if it is an existing appointment
        #   # else, there's already dynamic JS to add / remove new appointments
        #   s.input :_destroy, :as => :boolean, :label => "Destroy?"
        # end
      end
      # f.nested_fields_for :specifications do |ff|
      #   ff.remove_nested_fields_link
      #   ff.text_field :name
      # end
      # f.add_nested_fields_link :specifications

      f.input :category
      f.input :brand
      # f.input :article
      # f.input :product_code
      # f.input :supplier
      f.input :description, :as => :ckeditor
      f.input :meta_keywords
      f.input :meta_description

    end
    f.actions
  end


end
