# encoding: UTF-8
ActiveAdmin.register Post do

  controller do

    def create
      create! do |format|
        format.html { redirect_to admin_posts_url}
      end
    end

    def update
      update! do |format|
        format.html { redirect_to admin_posts_url}
      end
    end
  end

  index do
    selectable_column
    id_column
    column :title
    column :title_image do |item|
      image_tag(item.title_image.url(:thumb), :height => '100')
    end

    column :admin
    column :tag_list

    column :description do |item|
      truncate(item.description, :length => 300, :omission => '...')
    end
    column :created_at do |item|
      Russian::strftime(item.created_at, '%d %B %Y')
    end
    actions
  end

  filter :title
  filter :created_at
  # filter :current_sign_in_at
  # filter :sign_in_count
  # filter :created_at

  form html: { multipart: true }  do |f|
    f.inputs 'Новая новость' do
      f.input :title
      f.input :title_image, :image_preview => true
      f.input :admin
      f.input :tag_list,
              label: 'Тэги',
              input_html: {
                  data: {
                      placeholder: 'Теги',
                      saved: f.object.tags.map{|t| {id: t.name, name: t.name}}.to_json,
                      url: autocomplete_tags_path },
                  class: 'tagselect'
              }
      f.input :description, :as => :ckeditor

    end
    f.actions
  end

  controller do
    def autocomplete_tags
      @tags = ActsAsTaggableOn::Tag.
          where('name LIKE ?', "#{params[:q]}%").
          order(:name)
      respond_to do |format|
        format.json { render json: @tags , :only => [:id, :name] }
      end
    end
  end


end
