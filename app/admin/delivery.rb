# encoding: UTF-8
ActiveAdmin.register Delivery do

  controller do

    def create
      create! do |format|
        format.html { redirect_to admin_deliveries_url}
      end
    end

    def update
      update! do |format|
        format.html { redirect_to admin_deliveries_url}
      end
    end
  end

  index do
    selectable_column
    id_column
    column :name
    column :logo do |item|
      image_tag(item.logo.url(:thumb), :height => '100')
    end
    column :description do |item|
      truncate(item.description, :length => 300, :omission => '...')
    end

    actions
  end

  filter :title
  # filter :current_sign_in_at
  # filter :sign_in_count
  # filter :created_at

  form html: { multipart: true }  do |f|
    f.inputs  do
      f.input :name
      f.input :description, :as => :ckeditor
      f.input :logo
    end
    f.actions
  end


end
