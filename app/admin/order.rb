# encoding: UTF-8
ActiveAdmin.register Order do

  scope :all

  scope :wait_payment, :default => true do
    Order.where(:status => 'wait_payment')
  end

  scope :is_paid do
    Order.where(:status => 'is_paid')
  end

  scope :in_process do
    Order.where(:status => 'in_process')
  end

  scope :processed do
    Order.where(:status => 'processed')
  end

  controller do

    def create
      create! do |format|
        format.html { redirect_to admin_orders_url}
      end
    end

    def update
      update! do |format|
        format.html { redirect_to admin_orders_url}
      end
    end
  end

  show do
    # renders app/views/admin/posts/_some_partial.html.erb
    render 'show_order_items'
  end
  
  controller do
    def scoped_collection
      Order.without_new
    end
  end

  index do
    selectable_column
    id_column
    column :status
    column :status do |i|
      best_in_place i, :status, as: :select, :collection => Order::STATUSES, url: [:admin, i]
    end
    column :phone do |i|
      best_in_place i, :phone, as: :input, url: [:admin, i]
    end
    column :address do |i|
      best_in_place i, :address, as: :input, url: [:admin, i]
    end
    column :fio do |i|
      best_in_place i, :fio, as: :input, url: [:admin, i]
    end
    column :wishes do |i|
      best_in_place i, :wishes, as: :textarea, url: [:admin, i]
    end
    column :email
    column :user

    # column :items do |items|
    #   items
    # end

    column "items" do |item|
      item.items.map { |i| i.product.name }.compact
    end

    column :delivery
    column :delivery do |i|
      best_in_place i, :delivery_id, :type => :select, :collection => Delivery.select('id, name').to_a.each_with_object({}){ |c,h| h[c.id] = c.name }, url: [:admin, i]
    end
    column :created_at do |item|
      Russian::strftime(item.created_at, '%d %B %Y')
    end
    actions
  end

  filter :email
  filter :status
  filter :phone
  filter :address
  filter :fio
  filter :wishes
  filter :user
  filter :delivery
  filter :created_at
  # filter :current_sign_in_at
  # filter :sign_in_count
  # filter :created_at

  form do |f|
    f.inputs 'Редактирование заказа' do
      f.input :email
      f.input :status
      f.input :delivery
      f.input :phone
      f.input :address
      f.input :fio
      f.input :wishes
    end
    f.actions
  end


end
