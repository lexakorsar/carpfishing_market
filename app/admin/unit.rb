# encoding: UTF-8
ActiveAdmin.register Unit do

  menu :parent => 'Технические харрактеристики'

  controller do

    def create
      create! do |format|
        format.html { redirect_to admin_units_url}
      end
    end

    def update
      update! do |format|
        format.html { redirect_to admin_units_url}
      end
    end
  end

  index do
    selectable_column
    id_column
    column :name

    actions
  end

  filter :name
  # filter :current_sign_in_at
  # filter :sign_in_count
  # filter :created_at

  form do |f|
    f.inputs 'Единицы измерения' do
      f.input :name
    end
    f.actions
  end


end
