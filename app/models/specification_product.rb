class SpecificationProduct < ActiveRecord::Base
  belongs_to :product
  belongs_to :specification

  attr_accessible :product_id, :specification_id, :value
end
