class News < ActiveRecord::Base
  attr_accessible :title, :description, :title_image

  has_attached_file :title_image,
                    :styles => {
                        :big => '600x600>',
                        :medium => "300x300>",
                        :thumb => "100x100>"
                    },
                    :default_url => ActionController::Base.helpers.asset_path("#{Rails.application.config.assets.prefix}/missing_:style.png")

  validates_attachment_content_type :title_image, :content_type => /\Aimage\/.*\Z/
end
