# encoding: UTF-8
class AlbumImage < ActiveRecord::Base
  belongs_to :album

  attr_accessible :image,
                  :name,
                  :albums_id,
                  :album

  has_attached_file :image,
                    :styles => {
                        :big => '1920×1080>',
                        :medium => '600x600>',
                        :thumb => '100x100>'
                    },
                    :default_url => ActionController::Base.helpers.asset_path("#{Rails.application.config.assets.prefix}/missing_:style.png")

  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
