class Delivery < ActiveRecord::Base
  has_many :orders
  attr_accessible :name, :description, :logo

  has_attached_file :logo,
                    :styles => {
                        :big => '600x600>',
                        :medium => '300x300>',
                        :thumb => '100x100>'
                    },
                    :default_url => ActionController::Base.helpers.asset_path("#{Rails.application.config.assets.prefix}/missing_:style.png")

  validates_attachment_content_type :logo, :content_type => /\Aimage\/.*\Z/
end
