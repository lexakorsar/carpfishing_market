require 'elasticsearch/model'
class Product < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  belongs_to :category
  belongs_to :brand
  belongs_to :supplier

  before_destroy :save_product_names_for_orders

  has_many :order_items, dependent: :nullify
  has_many :specification_products
  has_many :specifications, :through => :specification_products

  accepts_nested_attributes_for :specifications, allow_destroy: true
  accepts_nested_attributes_for :specification_products, allow_destroy: true

  attr_accessible :name,
                  :price,
                  :description,
                  :category,
                  :category_id,
                  :title_image,
                  :brand,
                  :brand_id,
                  :site_url,
                  :article,
                  :product_code,
                  :supplier_id,
                  :rich_file_id,
                  :is_active,
                  :hight,
                  :width,
                  :len_metr,
                  :len_fut,
                  :count,
                  :rod_test,
                  :color,
                  :flavor,
                  :is_hit,
                  :is_sale,
                  :specification_ids,
                  :specification_products_attributes,
                  :fake_price,
                  :meta_keywords,
                  :meta_description,
                  :currency


  has_attached_file :title_image,
                    :styles => {
                        :big => '450x450>',
                        :medium => '300x300>',
                        :small => '170x170>',
                        :thumb => '100x100>'
                    },
                    :default_url => ActionController::Base.helpers.asset_path("#{Rails.application.config.assets.prefix}/missing_:style.png")

  validates_attachment_content_type :title_image, :content_type => /\Aimage\/.*\Z/

  settings index: { number_of_shards: 1 } do
    mappings dynamic: 'false' do
      indexes :id,        type: 'long', index: :not_analyzed
      indexes :name,     analyzer: 'snowball'
      indexes :description,   analyzer: 'snowball'
      indexes :brand_id,   type: 'long', index: :not_analyzed, include_in_all: false
      indexes :category_id,   type: 'long', index: :not_analyzed, include_in_all: false
    end
  end

  def self.search(query)
    __elasticsearch__.search(
        {
            query: {
                multi_match: {
                    query: query,
                    fields: %w(name description brand.title category.title)
                }
            }
        }
    )
  end

  def as_indexed_json(options={})
    self.as_json(
      include: { category: { only: :title},
                 brand:  { only: :title}
      })
  end

  # after_save    { Indexer.perform_async(:index,  self.id) }
  # after_destroy { Indexer.perform_async(:delete, self.id) }

  private

  def save_product_names_for_orders
    self.order_items.update_all(product_name: self.name)
  end
end
