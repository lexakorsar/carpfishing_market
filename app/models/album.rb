# encoding: UTF-8
class Album < ActiveRecord::Base
  has_many :album_images

  attr_accessible :title,
                  :description,
                  :album_images,
                  :album_images_attributes

  accepts_nested_attributes_for :album_images, :allow_destroy => true
end
