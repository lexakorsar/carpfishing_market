require 'spreadsheet'

class Supplier < ActiveRecord::Base
  after_save :start_parse
  has_many :products

  attr_accessible :name, :description, :type_price, :price

  has_attached_file :price

  validates_attachment_content_type :price, :content_type => %w(application/pdf application/vnd.ms-excel application/vnd.openxmlformats-officedocument.spreadsheetml.sheet application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document text/plain)

  def start_parse
    if Rails.env.development?
      parse_price
    else
      Delayed::Job.enqueue(ParsePrice.new(self.id))
    end
  end

  def parse_price
    if self.price.present?
      Spreadsheet.client_encoding = 'UTF-8'
      price = Spreadsheet.open (self.price.path)
      sheet1 = price.worksheet 0

      sheet1.each 11 do |row|
        if row[2] != nil
          Product.create(
              :name => row[1],
              :article => row[2],
              :product_code => row[3],
              :site_url => row[4],
              :price => row[5],
              :supplier => self
          )
        end
      end

    end
  end
end
