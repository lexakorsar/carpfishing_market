class Post < ActiveRecord::Base
  acts_as_taggable

  # has_many :post_images

  belongs_to :admin

  attr_accessible :title, :description, :title_image, :admin_id, :tag_list, :tag_ids

  has_attached_file :title_image,
                    :styles => { :medium => '300x300>', :thumb => '100x100>'}
  # :url => "/attachments/:id/title_image/:basename_:style.:extension",
  # :path => ":rails_root/public/attachments/:id/title_image/:basename_:style.:extension"

  # :default_url => "/images/:style/missing.png"

  validates_attachment_content_type :title_image, :content_type => %w(image/jpg image/jpeg image/png image/gif)

  scope :by_join_date, -> {
    order('created_at DESC')
  }

end
