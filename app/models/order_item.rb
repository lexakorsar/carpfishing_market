class OrderItem < ActiveRecord::Base
  belongs_to :product
  belongs_to :order

  validates_presence_of :product
  validates_presence_of :order
  validates_presence_of :price, if: :need_price?

  scope :with_product, ->(product){ where(product: product) }

  after_initialize :init_product,if: :new_record?

  def count=(new_count)
    super
    set_sum
  end

  def product=(new_product)
    super
    init_product
  end

  protected

  def init_product
    if self.product.present?
      self.price = product.price * (1 - self.order.user.discount)
      set_sum
    end
  end

  def set_sum
    self.sum = self.price * self.count
  end

  def need_price?
    ! self.order.is_new?
  end
end
