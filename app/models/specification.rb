class Specification < ActiveRecord::Base
  has_many :specification_products
  has_many :products, :through => :specification_products

  attr_accessible :name, :units

end
