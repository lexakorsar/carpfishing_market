# encoding: UTF-8
class Order < ActiveRecord::Base
  belongs_to :user
  belongs_to :delivery
  has_many :items, class_name: 'OrderItem'
  validate :validate_status

  validates_presence_of :phone, if: :validate_contacts
  validates_presence_of :address, if: :validate_contacts

  attr_accessor :validate_contacts
  attr_accessible :fio, :address, :wishes, :phone, :email, :user, :delivery, :delivery_id, :status

  STATUSES = %w(new wait_payment is_paid in_process processed)

  STATUSES_VIEW_INFO = {
      new: 'Новый',
      wait_payment: 'Ожидает оплаты',
      is_paid: 'Оплачен',
      in_process: 'Выполняется',
      processed: 'Выполнен'
  }

  STATUSES_CLASS_INFO = {
      new: 'danger',
      wait_payment: 'warning',
      is_paid: 'active',
      in_process: 'info',
      processed: 'success'
  }

  NEW_STATUS = 'new'
  WAIT_PAYMENT_STATUS = 'wait_payment'

  scope :cart, -> {where(:status => NEW_STATUS)}
  scope :without_new, -> {where.not(status: NEW_STATUS)}
  scope :wait_payment, -> {where(status: WAIT_PAYMENT_STATUS)}
  # default_scope {where.not(status: NEW_STATUS)}

  STATUSES.each do |status|
    define_method "is_#{status}?" do
      self.status.eql? status
    end
  end

  def set_confirmed_by_user
    self.status = WAIT_PAYMENT_STATUS
    self.save
  end

  def add_product(product, count)

    current_item = self.find_or_create_product product

    current_item.count += count.to_i

    if current_item.valid?
      current_item.save
    end

  end

  def remove_product(product)

    current_item = self.find_product product

    if current_item.present?
      current_item.destroy
    end

  end


  def decrease_product_count(product, count)

    current_item = self.find_product product

    if current_item.present?
      current_item.count -= count.to_i
      if current_item.count <= 0
        current_item.destroy
      else
        if current_item.valid?
          current_item.save
        end
      end
    end

  end

  def update_product_count(product, count)
    if count.to_i >= 0
      item = self.find_or_create_product product
      item.count = count.to_i
      if item.valid?
        item.save
      end
    else
      item = self.find_product product
      item.destroy if item.present?
    end
  end

  def get_sum
    self.items.sum(:sum)
  end

  protected



  def find_product(product)
    self.items.with_product(product).first
  end

  def find_or_create_product(product)
    item = self.find_product product
    if item.nil?
      item = self.items.with_product(product).new
    end

    item
  end

  def validate_status
    errors.add(:status, :status_incorrect) unless STATUSES.include? self.status
  end

end
