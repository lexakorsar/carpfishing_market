class Category < ActiveRecord::Base
  default_scope { where(:is_show => true).order('sorting') }
  include TheSortableTree::Scopes


  acts_as_nested_set
  attr_accessible :title, :description, :parent_id, :title_image, :sorting, :is_show, :meta_keywords, :meta_description

  has_attached_file :title_image,
                    :styles => {
                        :big => '600x600>',
                        :medium => '300x300>',
                        :thumb => '100x100>'
                    },
                    :default_url => ActionController::Base.helpers.asset_path("#{Rails.application.config.assets.prefix}/missing_:style.png")

  validates_attachment_content_type :title_image, :content_type => /\Aimage\/.*\Z/

  has_many :products

  def branch_ids
    self_and_descendants.map(&:id).uniq
  end

  def all_products
    Product.where(:category_id => branch_ids, :is_active => true)#.where('count > 0')
  end

  def all_category_products
    Product.where(:category_id => self.id, :is_active => true)
  end

  def all_products_for_admin
    Product.where(:category_id => branch_ids)
  end
end
