#= require active_admin/base
#= require best_in_place
#= //require best_in_place.jquery-ui
#= require jquery.purr
#= require best_in_place.purr

#= require select2


$(document).ready ->
  $(".best_in_place").best_in_place();

  $(".tagselect").each ->
    placeholder = $(this).data("placeholder")
    url = $(this).data("url")
    saved = $(this).data("saved")
    $(this).select2
      tags: true
      placeholder: placeholder
      minimumInputLength: 1
      initSelection: (element, callback) ->
        saved and callback(saved)
        return

      ajax:
        url: url
        dataType: "json"
        data: (term) ->
          q: term

        results: (data) ->
          results: data

      createSearchChoice: (term, data) ->
        if $(data).filter(->
          @name.localeCompare(term) is 0
        ).length is 0
          id: term
          name: term

      formatResult: (item, page) ->
        item.name

      formatSelection: (item, page) ->
        item.name

    return

  CKEDITOR.replace "ckeditor"  if $("#ckeditor").length
  $("#ckeditor").prev("label").css "float", "none"  if $("#ckeditor").prev("label").length
  return


