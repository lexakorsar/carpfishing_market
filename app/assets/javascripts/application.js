// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require bootstrap.min
//= require jquery.validate.min
//= require jquery-validate.bootstrap-tooltip.min
//= require additional-methods.min
//= require localization/messages_ru.min
//= require nprogress
//= require ckeditor/init
//= require nested_form_fields
//= require superfish
//= require jquery.magnific-popup
//= require turbolinks
//= require_this

var ready;
ready = function() {

    $.widget( "custom.catcomplete", $.ui.autocomplete, {
        _create: function() {
            this._super();
            this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
        },
        _renderMenu: function( ul, items ) {
            var that = this,
                currentCategory = "";
            $.each( items, function( index, item ) {
                var li;
                if ( item.category != currentCategory ) {
                    ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                    currentCategory = item.category;
                }
                li = that._renderItemData( ul, item );
                if ( item.category ) {
                    li.attr( "aria-label", item.category + " : " + item.label );
                }
            });
        }
    });

    var data = [
        { label: "anders", category: "" },
        { label: "andreas", category: "" },
        { label: "antal", category: "" },
        { label: "annhhx10", category: "Products" },
        { label: "annk K12", category: "Products" },
        { label: "annttop C13", category: "Products" },
        { label: "anders andersson", category: "People" },
        { label: "andreas andersson", category: "People" },
        { label: "andreas johnson", category: "People" }
    ];

    var cache = {};

    $( ".search" ).catcomplete({
        delay: 0,
        source: function( request, response ) {
            var term = request.term;
            if ( term in cache ) {
                response( cache[ term ] );
                return;
            }
            $.getJSON( "/shop/autocomplete", request, function( data, status, xhr ) {
                cache[ term ] = data;
                response( data );
            });
        },
        select: function(event, ui) {
            $("#search-form .search").val(ui.item.label);
            $("#search-form").submit(); }
    });


    $("#new_user").validate({
        errorClass: "has-error has-feedback",
        validClass: "has-success has-feedback",

        rules: {
            "user[email]": { required: true, email: true},
            "user[password]": {required: true, minlength: 8, maxlength: 128},
            "user[password_confirmation]": {required: true, equalTo: "#user_password"},
            "offer_agry": {required: true}

        },

        messages: {
            "user[password_confirmation]": "Пароли не совпадают",
            "offer_agry": "Необходимо принять пользовательское соглашение"
        },

        highlight: function (element, errorClass, validClass) {
            $(element).closest(".form-group").addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).closest(".form-group").addClass(validClass).removeClass(errorClass);
        }
    });

    $(".order-form").validate({
        errorClass: "has-error has-feedback",
        validClass: "has-success has-feedback",

        rules: {
            "order[email]": {required: true, email: true},
            "order[phone]": {required: true},
            "order[fio]": {required: true},
            "order[address]": {required: true}
        },

        highlight: function (element, errorClass, validClass) {
            $(element).closest(".form-group").addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).closest(".form-group").addClass(validClass).removeClass(errorClass);
        }
    });


    $('.add-to-cart').on('click', function () {
        var cart = $('.glyphicon-shopping-cart');

        var img = $(this).parent();
        while(img.attr("prd") != 'prd') {
            img = img.parent();
        }
//        var imgtodrag = $(this).parent('.prd').find("img").eq(0);
        var imgtodrag = img.find("img");
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                    top: imgtodrag.offset().top,
                    left: imgtodrag.offset().left
                })
                .css({
                    'opacity': '0.7',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '100'
                })
                .appendTo($('body'))
                .animate({
                    'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75,
                    'height': 75
                }, 400, '');

//            setTimeout(function () {
//                cart.effect("shake", {
//                    times: 2
//                }, 200);
//            }, 1500);

            imgclone.animate({
                'width': 0,
                'height': 0
            }, function () {
                $(this).detach()
            });
        }
    });


    $("ul.js_navigation").superclick({
        pathClass:	'active',
        delay:       800,                            // one second delay on mouseout
        animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation
        animationOut:   {opacity:'hide',height:'hide'},  // fade-in and slide-down animation
        speed:       'normal',                          // faster animation speed
        autoArrows:  true
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#scroll-top').fadeIn();
        } else {
            $('#scroll-top').fadeOut();
        }
    });

    $('#scroll-top').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    $('#main-carousel').carousel();

    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">Изображение #%curr%</a> не может быть загруженно.',
            titleSrc: function(item) {
                return item.el.attr('data-title');
            }
        }
    });

};

$(document).ready(ready);
$(document).on('page:load', ready);
$(document).on('page:fetch',   function() { NProgress.start(); });
$(document).on('page:change',  function() { NProgress.done(); });
$(document).on('page:restore', function() { NProgress.remove(); });



