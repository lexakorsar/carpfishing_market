class Shop::OrderController < BaseController

  before_filter :authenticate_user!, only: :index

  skip_before_filter :verify_authenticity_token, :only => %i(add remove update_count decrease)

  before_filter :get_order, except: %i(index)
  before_filter :get_count, :only => %i(add update_count decrease)
  before_filter :get_product, :only => %i(add remove update_count decrease)

  def index #архив
    @orders = current_or_guest_user.orders #todo: представления
  end

  def add

    @order.add_product(@product, @count)

    respond_to do |format|
      format.html {redirect_to action: :show, product_id: @product.id }
      format.js
    end

  end

  def remove
    @order.remove_product(@product)
    render action: :update_count
  end

  def update_count
    @order.update_product_count @product, @count
  end

  def decrease
    @order.decrease_product_count(@product, @count)
  end

  def show
    # unless user_signed_in?
    #   flash[:notice] = 'Для оформления заказа требуется авторизация'
    #   authenticate!
    # end
  end

  def edit
    @order.phone = current_or_guest_user.phone
    @order.fio = current_or_guest_user.fio
    @order.address = current_or_guest_user.address
  end

  def update
    @order.attributes = params[:order]
    @order.validate_contacts = true

    if @order.valid?
      @order.save
      redirect_to action: :confirm
    else
      render action: :edit
    end
  end

  def confirm

  end

  def to_process
    @order.set_confirmed_by_user
    if current_or_guest_user && current_or_guest_user.lazy_id.present?

      # user.destroy
      # session[:guest_user_id] = nil

      @pass = get_temp_password
      @test_u = User.find_by_email(@order.email)
      if @test_u.nil?
        @u = User.create(email: @order.email, :password => @pass, :fio => @order.fio, :address => @order.address, :phone => @order.phone )
        @u.save(validate: false)
      else
        @u = @test_u
      end

      @order.user = @u
      if @order.save!
        guest_user.destroy # Stuff have been handed over. Guest isn't needed anymore.
        cookies.delete :uuid # The cookie is also irrelevant now
        UserMailer.auto_registration_email(@u, @pass).deliver if @test_u.nil?
      end
      sign_in(:user, @u)
    end
    flash[:notice] = 'Ваш заказ оформлен. В ближайшее время с Вами свяжется наш менеджер'
    UserMailer.delay.order_notification(@order)
    UserMailer.delay.order_information(@order)
    redirect_to controller: 'shop/main', action: :index
  end

  private


  def get_temp_password
    o =  [('a'..'z'),('A'..'Z')].map{|i| i.to_a}.flatten
    (0...9).map{ o[rand(o.length)]  }.join

  end

  def get_order
    if current_or_guest_user.present?
      @order = current_or_guest_user.cart
    else
      @order = create_guest_user.cart
    end

  end

  def get_product
    @product = Product.find(params[:product_id])
  end

  def get_count
    @count = params[:count].present? ? params[:count].to_i : 1
  end
end
