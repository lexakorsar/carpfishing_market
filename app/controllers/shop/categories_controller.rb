class CategoriesController < BaseController
  include TheSortableTreeController::Rebuild
  inherit_resources

  def manage
    @categories = Category.nested_set.select('id, title, parent_id').all
  end

  def index
    # redirect_to shop_category_products_url(Category.roots.first)
  end

  def show
    # redirect_to shop_category_products_url(params[:id])
  end
end
