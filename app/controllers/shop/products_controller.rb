class Shop::ProductsController < BaseController
  before_filter :get_category
  skip_before_filter  :get_category, :only => [:search, :autocomplete]

  def index
    if params[:brand_id]
      @products = Category.find(params[:category_id]).all_products.where(:brand_id => params[:brand_id]).paginate(:page => params[:page], :per_page => 30)

    else
      @products = Category.find(params[:category_id]).all_products.paginate(:page => params[:page], :per_page => 30)

    end
  end

  def search
    if params[:q].nil?
      @products = []
    else
      @products = Product.search(params[:q]).records.where('category_id IS NOT NULL').where(:is_active => true)
    end

  end

  def autocomplete
    render json: Product.search(params[:term]).records.where('category_id IS NOT NULL').where(:is_active => true).limit(10).map{ |x| {:label => x.name, :category => x.category.title}}
  end

  def show
    @product = Product.find(params[:id])
  end

  def get_category
    @category = Category.find(params[:category_id])
  end
end
