class Shop::BrandsController < BaseController
  def show
    @products = Brand.find(params[:id]).all_products
  end
end
