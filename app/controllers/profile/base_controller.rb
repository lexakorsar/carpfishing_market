class Profile::BaseController < ApplicationController
  layout 'profile'
  before_action :authenticate_user!
end
