class Profile::OrdersController < Profile::BaseController
  def index
    @user_orders =  current_user.orders.without_new.order('created_at DESC')
  end

  def show
    @order = Order.find(params[:id])
  end
end
