class ApplicationController < ActionController::Base
  # include AuthorizationHelper
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_or_guest_user

  def current_or_guest_user
    if current_user
      if cookies[:uuid]
        logging_in # Look at this method to see how handing over works
        guest_user.destroy # Stuff have been handed over. Guest isn't needed anymore.
        cookies.delete :uuid # The cookie is also irrelevant now
      end
      current_user
    else
      guest_user
    end
  end

  def guest_user
    User.find_by_lazy_id(cookies[:uuid].nil? ? create_guest_user.lazy_id : cookies[:uuid])
  end

  private

  def logging_in
    guest_user.orders.update_all(user_id: current_user.id)
  end

  def create_guest_user
    uuid = rand(36**64).to_s(36)
    temp_email = "guest_#{uuid}@email_address.com"
    u = User.create(:email => temp_email, :lazy_id => uuid)
    u.save(:validate => false)
    cookies[:uuid] = { :value => uuid, :path => '/', :expires => 5.years.from_now }
    u
  end
  #todo: кронтаск который будет удалять "зависшие" заказы. т.е. заказы у которых нет пользователя
end
