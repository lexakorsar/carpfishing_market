class MainController < BaseController
  def index
    @hit_products = Product.where('category_id IS NOT NULL').where(:is_active => true, :is_hit => true).limit(4)
    @sale_products = Product.where('category_id IS NOT NULL').where(:is_active => true, :is_sale => true).limit(4)
    @comlect_products = Product.where('category_id IS NOT NULL').where(:is_active => true).limit(4)
  end

  def contacts

  end
  def bonus

  end

  def cards

  end

  def actions

  end

  def public_offer

  end

  def delivery

  end

  def payment

  end

  def order_create

  end


end
