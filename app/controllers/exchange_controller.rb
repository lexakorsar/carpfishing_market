class ExchangeController < ApplicationController
  skip_before_filter :verify_authenticity_token

  include ImportFromC
  include ExportToC
  TMP_DIR = '/tmp/upload'

  FILE_SIZE_LIMIT = 5242880

  def exchange
    unless valid_password?
      render text: "failure\nerror_authorize";
      return
    end

    case params[:mode]
      when 'checkauth' then render text: "success\n#{Rails.application.config.session_options[:key]}\n#{request.session_options[:id]}\n"
      when 'init' then

        render text: "zip=no\nfile_limit=#{FILE_SIZE_LIMIT}\n"
        clear_directory(TMP_DIR)

        session.delete(:import_status)

      else
        if params[:type].eql? 'catalog'
          filename = "#{TMP_DIR}/#{params[:filename]}"

          unless Dir.exist? File.dirname(filename)
            FileUtils.mkdir_p File.dirname(filename)
          end

          base_name = File.basename filename

          if params[:mode].eql? 'file'
            content = request.raw_post
            File.open(filename, 'ab'){ |f| f.write content }
            render text: "success\n"
          elsif params[:mode].eql? 'import'
            unless session[:import_status].present?
              session[:import_status] = Hash.new
            end

            if session[:import_status][params[:filename]].blank?
              if File.exist? filename
                if Rails.env.development?
                  load_cml_from_file(filename)
                else
                  Delayed::Job.enqueue(ImportC.new(filename), :queue => 'import')
                end

                session[:import_status][params[:filename]] = 'success'
              else
                session[:import_status][params[:filename]] = "failure\nerror_file_read $filename"
              end

              render text: "#{session[:import_status][params[:filename]]}\n"
            else
              render text: "#{session[:import_status][params[:filename]]}\n"
            end
          end

        elsif params[:type].eql? 'sale'
          if params[:mode].eql? 'query'
            xml = upload_orders
            render text: "#{xml}\n"
          elsif params[:mode].eql? 'success'
            confirm_upload
            render text: "success\n"
          elsif params[:mode].eql? 'file'
            render text: "success\n"
          end
        end
    end
  end

  protected

  def valid_password?
    if request.headers['HTTP_AUTHORIZATION'].present?
      email, password = Base64.decode64(request.headers['HTTP_AUTHORIZATION'].match(/Basic\s+(.*)$/)[1]).split(':')
      admin = Admin.where(email: email).first
      admin.present? && admin.valid_password?(password)
    else
      false
    end
  end

end
