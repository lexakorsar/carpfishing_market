class BlogController < BaseController
  before_filter :tag_cloud

  def tag_cloud
    @tags = Post.tag_counts_on(:tags)
  end

  def index
    @posts = Post.paginate(:page => params[:page], :per_page => 30).order('created_at DESC')

  end

  def show
    @post = Post.find(params[:id])
  end

  def tag_show
    @posts = Post.tagged_with(params[:id]).paginate(:page => params[:page], :per_page => 30).order('created_at DESC')
    # render :action => :index
  end
end
