set :stage, :production

set :deploy_to, '~/apps/ryst'

set :branch, 'master'

set :rails_env, 'production'

# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
server 'carplux.ru', user: 'tarasov_dev', roles: %w{web app db}

# role :app, %w{tarasov_dev@78.46.93.248}
# role :web, %w{tarasov_dev@78.46.93.248}
# role :db,  %w{tarasov_dev@78.46.93.248}