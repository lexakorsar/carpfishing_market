Rails.application.routes.draw do
  root 'shop/main#index'

  get '/projects', to: 'projects#index'

  get '/blog', to: 'blog#index'
  get '/blog/:id', to: 'blog#show', as: 'post'

  get '/blog//tag/:id', to: 'blog#tag_show', as: 'tag'

  match '/users/:id/finish_signup' => 'users#finish_signup', via: [:get, :patch], :as => :finish_signup

  mount Ckeditor::Engine => '/ckeditor'

  resources :news, :only => %w(index show)

  get 'contacts' => 'main#contacts'
  get 'bonus' => 'main#bonus'
  get 'cards' => 'main#cards'
  get 'actions' => 'main#actions'
  get 'public_offer' => 'main#public_offer'
  get 'delivery' => 'main#delivery'
  get 'payment' => 'main#payment'
  get '/admin/autocomplete_tags',
      to: 'admin/posts#autocomplete_tags',
      as: 'autocomplete_tags'

  namespace :gallery do
    root :to => 'main#index'
  end

  namespace :shop do
    root :to => 'main#index'
    get 'search', to: 'products#search'
    get 'autocomplete', to: 'products#autocomplete'
    resources :brands, only: %w(show)
    resources :categories, only: %w(index show) do
      resources :products, only: %w(index show)
    end

    scope :order do
      post ':product_id/add', to: 'order#add'
      post ':product_id/remove', to: 'order#remove'
      post ':product_id/update_count', to: 'order#update_count'
      post ':product_id/decrease', to: 'order#decrease'
      post ':product_id/update', to: 'order#update'
      post ':product_id/confirm', to: 'order#confirm'

      get 'show', to: 'order#show'
      get 'edit', to: 'order#edit'
      patch 'update', to: 'order#update'
      get 'confirm', to: 'order#confirm'
      post 'confirm', to: 'order#to_process'
      get :index
    end

  end



  devise_for :users, :controllers => { omniauth_callbacks: 'omniauth_callbacks' }

  namespace :profile do

    root :to => 'main#index'
    resources :orders, only: %w(index show)
    #     # Directs /admin_old/products/* to Admin::ProductsController
  end


  post 'exchange', to: 'exchange#exchange'
  get 'exchange', to: 'exchange#exchange'

  devise_for :admins, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

end
