# -*- coding: utf-8 -*-
# Configures your navigation
SimpleNavigation::Configuration.run do |navigation|
  # Specify a custom renderer if needed.
  # The default renderer is SimpleNavigation::Renderer::List which renders HTML lists.
  # The renderer can also be specified as option in the render_navigation call.
  # navigation.renderer = Your::Custom::Renderer

  # Specify the class that will be applied to active navigation items.
  # Defaults to 'selected' navigation.selected_class = 'your_selected_class'

  # Specify the class that will be applied to the current leaf of
  # active navigation items. Defaults to 'simple-navigation-active-leaf'
  # navigation.active_leaf_class = 'your_active_leaf_class'

  navigation.selected_class = 'active'

  # Item keys are normally added to list items as id.
  # This setting turns that off
  # navigation.autogenerate_item_ids = false

  # You can override the default logic that is used to autogenerate the item ids.
  # To do this, define a Proc which takes the key of the current item as argument.
  # The example below would add a prefix to each key.
  # navigation.id_generator = Proc.new {|key| "my-prefix-#{key}"}

  # If you need to add custom html around item names, you can define a proc that
  # will be called with the name you pass in to the navigation.
  # The example below shows how to wrap items spans.
  # navigation.name_generator = Proc.new {|name, item| "<span>#{name}</span>"}

  # The auto highlight feature is turned on by default.
  # This turns it off globally (for the whole plugin)
  # navigation.auto_highlight = false

  # If this option is set to true, all item names will be considered as safe (passed through html_safe). Defaults to false.
  # navigation.consider_item_names_as_safe = false

  # Define the primary navigation
  navigation.items do |primary|
    # Add an item to the primary navigation. The following params apply:
    # key - a symbol which uniquely defines your navigation item in the scope of the primary_navigation
    # name - will be displayed in the rendered navigation. This can also be a call to your I18n-framework.
    # url - the address that the generated item links to. You can also use url_helpers (named routes, restful routes helper, url_for etc.)
    # options - can be used to specify attributes that will be included in the rendered navigation item (e.g. id, class etc.)
    #           some special options that can be set:
    #           :if - Specifies a proc to call to determine if the item should
    #                 be rendered (e.g. <tt>if: -> { current_user.admin_old? }</tt>). The
    #                 proc should evaluate to a true or false value and is evaluated in the context of the view.
    #           :unless - Specifies a proc to call to determine if the item should not
    #                     be rendered (e.g. <tt>unless: -> { current_user.admin_old? }</tt>). The
    #                     proc should evaluate to a true or false value and is evaluated in the context of the view.
    #           :method - Specifies the http-method for the generated link - default is :get.
    #           :highlights_on - if autohighlighting is turned off and/or you want to explicitly specify
    #                            when the item should be highlighted, you can set a regexp which is matched
    #                            against the current URI.  You may also use a proc, or the symbol <tt>:subpath</tt>.
    #
    primary.item :root, 'Главная', root_url do |root_category|
    # primary.item :shop, 'Главная', shop_category_products_url(Category.roots.first), :highlights_on => /\/shop/ do |root_category|
      Category.roots.each do |root_cat|


        root_category.dom_class = 'nav nav-pills nav-stacked'
        root_category.item ":shop#{root_cat.id}", root_cat.title, shop_category_products_url(root_cat), :class => 'root_cat' do |categoty|
          if root_cat.children.present? && root_cat.children.size > 0
            root_cat.children.each do |cat|
              categoty.dom_class = 'nav nav-pills nav-stacked js_navigation ul_cat'
              categoty.item ":shop#{cat.id}", cat.title, shop_category_products_url(cat), :class => 'margin-10 cat' do |sub_category|
                if cat.children.present? && cat.children.size > 1
                  cat.children.each do |sub_cat|
                    sub_category.dom_class = 'nav nav-pills nav-stacked ul_sub_cat'
                    sub_category.item ":shop#{sub_cat.id}", sub_cat.title, shop_category_products_url(sub_cat), :class => 'margin-20 sub_cat'
                  end
                end
              end

            end
          end

        end

        # Category.each_with_level(cat.self_and_descendants) do |category, level|
        # case level
        #   when 1
        #     li_class = 'margin-10 cat'
        #   when 2
        #     li_class = 'margin-20 sub_cat'
        #   else
        #     li_class =' root_cat'
        # end
        #
        # categoty.dom_class = 'nav nav-pills nav-stacked'
        # categoty.item ":shop#{category.id}", category.title, shop_category_products_url(category), :class => li_class

        # end
      end
    end

    primary.item :news, 'События', news_index_url, :highlights_on => /\/news/
    # primary.item :gallery, 'Блог', blog_url, :highlights_on => /\/blog/
    primary.item :projects, 'Фотогалерея', projects_url, :highlights_on => /\/projects/
    primary.item :contacts, 'Контакты', contacts_url


    # you can also specify html attributes to attach to this particular level
    # works for all levels of the menu
    # primary.dom_attributes = {id: 'menu-id', class: 'menu-class'}

    primary.dom_class = 'nav nav-justified'

    # You can turn off auto highlighting for a specific level
    # primary.auto_highlight = false
  end
end
