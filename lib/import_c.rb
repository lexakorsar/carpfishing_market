class ImportC <  Struct.new(:file_name)
  include ImportFromC
  def perform
    load_cml_from_file(file_name)
  end
end