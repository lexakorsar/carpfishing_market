#encoding: utf-8
module ImportFromC
  TMP_DIR = '/tmp/upload'

  def load_offers(package)
    Product.update_all(price: 0, count: 0)

    offers = package.xpath('./Предложения')
    price_type_id = ''

    package.xpath('./ТипыЦен/ТипЦены').each do |price_type|
      price_type_id = price_type.xpath('./Ид').first.text if price_type.xpath('./Наименование').first.text.strip.eql? 'Розничная'
    end

    if offers.present?
      offers.first.xpath('./Предложение').each do |offer|
        product = Product.where(internal_id: offer.xpath('./Ид').first.text).first
        if product.present?
          offer.xpath('./Цены/Цена').each do |product_price|
            if product_price.xpath('./ИдТипаЦены').first.text.eql? price_type_id
              product.price = product_price.xpath('./ЦенаЗаЕдиницу').first.text.to_f
              product.fake_price = product.price  if product.price != '0.0'.to_f
              # product.is_active = true
            end
          end
          product.count = offer.xpath('./Количество').first.text.to_i if offer.xpath('./Количество').first.present?

          product.save
        end
      end
    end

  end

  def load_item(xml_product, classifier)

    properties = classifier.xpath('./Свойства/Свойство')

    category_properties = Hash.new

    properties.each do |property|
      property_id = property.xpath('./Ид').first.text
      property_name = property.xpath('./Наименование').first.text
      if %w(Категория Подкатегория Производитель).include? property_name
        category_properties[property_id] = Hash.new
        property.xpath('./ВариантыЗначений/Справочник').each do |variant|
          category_properties[property_id][variant.xpath('./ИдЗначения').first.text ] = variant.xpath('./Значение').first.text
        end
      end
    end

    internal_id = xml_product.xpath('./Ид').text
    product = Product.where(internal_id: internal_id).first

    if product.nil?
      product = Product.new
      product.internal_id = internal_id
    end

    # product.is_active = false

    unless xml_product.xpath('./Картинка').empty?
      file = File.open("#{TMP_DIR}/#{xml_product.xpath('./Картинка').text}")
      product.title_image = file
      file.close
    end


    # product.name = xml_product.xpath('./Наименование').first.text.strip
    product.original_name = xml_product.xpath('./Наименование').first.text.strip
    product.article = xml_product.xpath('./Артикул').first.text.strip if xml_product.xpath('./Артикул').first.present?

    unless xml_product.xpath('./Описание').empty?
      product.description = xml_product.xpath('./Описание').first.text
    end

    properties_hash = Hash.new

    xml_product.xpath('./ЗначенияСвойств/ЗначенияСвойства').each do |xml_property|
      properties_hash[xml_property.xpath('./Ид').first.text] = xml_property.xpath('./Значение').first.text
    end

    producer_property_id = 'd41e9390-42f2-11e4-8878-50e5498845d9'

    producer_id = properties_hash[producer_property_id]
    if producer_id.present?
      brand = Brand.where(internal_id: producer_id).first

      if brand.nil?
        brand_name = category_properties[producer_property_id][producer_id]
        brand = Brand.where(title: brand_name).first
        if brand.nil?
          brand = Brand.new
          brand.title = brand_name
          brand.save
        end
      end
    else
      brand = Brand.first
    end

    product.brand = brand

    subcategory_property_id = 'd41e9392-42f2-11e4-8878-50e5498845d9'
    category_property_id = 'd41e9391-42f2-11e4-8878-50e5498845d9'

    subcategory_id = properties_hash[subcategory_property_id]
    category_id = properties_hash[category_property_id]

    if category_id.present?
      category = Category.where(internal_id: category_id).first

      if category.nil?
        category_name = category_properties[category_property_id][category_id]
        category = Category.where(title: category_name).first
        if category.nil?
          category = Category.new
          category.title = category_name
          category.save
        end
      end

      if subcategory_id.present?
        subcategory = category.children.where(internal_id: subcategory_id).first

        if subcategory.nil?
          subcategory_name = category_properties[subcategory_property_id][subcategory_id]
          subcategory = category.children.where(title: subcategory_name).first

          if subcategory.nil?
            subcategory = category.children.new
            subcategory.title = subcategory_name
            category.save
          end
        end
      else
        subcategory = category
      end
    else
      subcategory = Category.first
    end

    product.category = subcategory

    product.save
  end

  def load_items(catalog, classifier)
    catalog.xpath('//Товары/Товар').each do |product|
      load_item product, classifier
    end

    #todo тут проставить новое поступление если надо
  end

  def load_cml(text)
    doc = Nokogiri::XML text

    classifier = doc.xpath('//Классификатор')

    catalog = doc.xpath('//Каталог')

    if catalog.present? # файл с товарами
      load_items catalog, classifier
    end

    package = doc.xpath '//ПакетПредложений'

    if package.present?
      load_offers package
    end
  end

  def load_cml_from_file(file_name)
    load_cml File.read(file_name)
  end

  def clear_directory(path)
    FileUtils.rm_rf path
  end
end