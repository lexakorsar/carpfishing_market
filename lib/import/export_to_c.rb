#encoding: utf-8
module ExportToC
  def confirm_upload
    Order.without_new.where(upload_confirmed: false).update_all(upload_confirmed: true)
  end

  def upload_orders

    orders  = Order.wait_payment.where(upload_confirmed: false)

    builder = Nokogiri::XML::Builder.new(encoding: 'CP1251'){ |xml|
          xml.КоммерческаяИнформация('ВерсияСхемы' => '2.04', 'ДатаФормирования' => Time.now.strftime('%Y-%m-%d %H:%M')) do
            orders.each do |order|
              xml.Документ{
                xml.Ид order.id
                xml.Номер order.id
                xml.Дата order.updated_at.strftime('%Y-%m-%d')
                xml.ХозОперация {'Заказ товара'}
                xml.Роль {'Продавец'}
                xml.Валюта {'руб'}
                xml.Курс '1'
                xml.Контрагенты{
                  xml.Контрагент{
                    xml.Ид order.user.id
                    xml.Наименование order.user.name || 'Розничный покупатель'
                    xml.Роль 'Покупатель'
                    xml.АдресРегистрации {
                      xml.Представление order.user.address
                    }
                    xml.Контакты{
                      xml.Контакт {
                        xml.Тип 'Почта'
                        xml.Значение order.user.email
                      }
                    }
                  }
                }
                xml.Время order.updated_at.strftime('%H:%M')
                xml.Товары{
                  order.items.each do |order_item|
                    xml.Товар{
                      xml.Ид order_item.product.internal_id
                      xml.БазоваяЕдиница('Код' => '796', 'НаименованиеПолное' => 'Штука', 'МеждународноеСокращение' => 'PCE') {'шт'}
                      xml.Наименование order_item.product_name
                      xml.ЦенаЗаЕдиницу order_item.price
                      xml.Количество order_item.count
                      xml.Сумма order_item.sum
                    }
                  end
                }
                xml.Сумма order.get_sum
                xml.ЗначенияРеквизитов {
                  xml.ЗначениеРеквизита {
                    xml.Наименование 'Метод оплаты'
                    xml.Значение 'Наличный расчет'
                  }
                  xml.ЗначениеРеквизита{
                    xml.Наименование 'Заказ оплачен'
                    xml.Значение 'false'
                  }
                  xml.ЗначениеРеквизита{
                    xml.Наименование 'Статус заказа'
                    xml.Значение '[N] Принят'
                  }
                }
              }
              order.uploaded = true
              order.save
            end
        end
    }

    builder.to_xml
  end
end