namespace :guest_data_sanitaze do
  desc 'Remove guest accounts more than a week old.'
  task :cleanup_users => :environment do
    User.where("lazy_id IS NOT NULL AND updated_at < ?", 1.week.ago).destroy_all
  end
end

namespace :guest_data_sanitaze do
  desc 'Remove new orders more than a week old.'
  task :cleanup_orders => :environment do
    Order.where(:status => 'new').where("updated_at < ?", 1.week.ago).destroy_all
  end
end
