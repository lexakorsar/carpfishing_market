class ParsePrice <  Struct.new(:supplier_id)
  def perform
    Supplier.find(supplier_id).parse_price
  end
end