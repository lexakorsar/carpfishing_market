class AddOriginalNameForProdutc < ActiveRecord::Migration
  def change
    change_table :products do |table|
      table.string :original_name
    end
  end
end
