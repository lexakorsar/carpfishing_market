class AddMetaTagsToCategoryAndProduct < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.text :meta_keywords
      t.text :meta_description
    end

    change_table :categories do |t|
      t.text :meta_keywords
      t.text :meta_description
    end
  end
end
