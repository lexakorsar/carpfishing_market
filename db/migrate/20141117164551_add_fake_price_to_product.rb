class AddFakePriceToProduct < ActiveRecord::Migration
  def change
    change_table :products do |table|
      table.string :fake_price
    end
  end
end
