class AddEmailToOrder < ActiveRecord::Migration
  def change
    change_table :orders do |table|
      table.string :email
    end
  end
end
