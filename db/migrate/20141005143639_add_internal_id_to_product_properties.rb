class AddInternalIdToProductProperties < ActiveRecord::Migration
  def change
    add_column :categories, :internal_id, :string, :null => false, :default => ''
    add_column :brands, :internal_id, :string, :null => false, :default => ''
  end
end
