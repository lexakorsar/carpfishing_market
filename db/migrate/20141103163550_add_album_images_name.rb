class AddAlbumImagesName < ActiveRecord::Migration
  def change
    change_table :album_images do |table|
      table.string :name
    end
  end
end
