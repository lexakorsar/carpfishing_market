class AddContactsForUser < ActiveRecord::Migration
  def change
    change_table :users do |table|
      table.string :fio
      table.text :address
      table.string :phone
    end
  end
end
