class AddSiteUrlToProduct < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.string :site_url
    end
  end
end
