class AddTechnicalCharecter < ActiveRecord::Migration
  def change
    change_table :products do |table|
      table.integer :hight
      table.integer :width
      table.integer :len_metr
      table.integer :len_fut
      table.integer :count
      table.string :rod_test
      table.string :color
      table.string :flavor
    end
  end
end
