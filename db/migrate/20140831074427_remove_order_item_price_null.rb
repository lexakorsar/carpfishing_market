class RemoveOrderItemPriceNull < ActiveRecord::Migration
  def change
    change_column_null :order_items, :price, false, 0.0

    change_column_default :order_items, :price, 0.0
  end
end
