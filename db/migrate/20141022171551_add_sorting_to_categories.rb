class AddSortingToCategories < ActiveRecord::Migration
  def change
    change_table :categories do |table|
      table.integer :sorting
      table.boolean :is_show, :default => true
    end
  end
end
