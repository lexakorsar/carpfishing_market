class CreateSpecificationProducts < ActiveRecord::Migration
  def change
    create_table :specification_products do |t|
      t.references :product, index: true
      t.references :specification, index: true
      t.string :value

      t.timestamps
    end
  end
end
