class AddDeliveryToOrder < ActiveRecord::Migration
  def change
    change_table :orders do |t|
      t.references :delivery, index: true
    end
  end
end
