class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :title
      t.text :description
      t.attachment :title_image

      t.timestamps
    end
  end
end
