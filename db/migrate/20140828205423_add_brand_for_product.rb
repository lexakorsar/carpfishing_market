class AddBrandForProduct < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.references :brand
    end
  end
end
