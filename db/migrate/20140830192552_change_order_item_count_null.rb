class ChangeOrderItemCountNull < ActiveRecord::Migration
  def change
    change_column_null :order_items, :count, false
    change_column_default :order_items, :count, 0
  end
end
