class AddAttachmentTitleImageToCategories < ActiveRecord::Migration
  def self.up
    change_table :categories do |t|
      t.attachment :title_image
    end
  end

  def self.down
    remove_attachment :categories, :title_image
  end
end
