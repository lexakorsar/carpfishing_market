class AddAttachmentTitleImageToProducts < ActiveRecord::Migration
  def self.up
    change_table :products do |t|
      t.attachment :title_image
    end
  end

  def self.down
    remove_attachment :products, :title_image
  end
end
