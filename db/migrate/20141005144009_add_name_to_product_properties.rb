class AddNameToProductProperties < ActiveRecord::Migration
  def change
    add_column :categories, :name, :string, :null => false, :default => ''
    add_column :brands, :name, :string, :null => false, :default => ''
  end
end
