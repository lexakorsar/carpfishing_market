class AddSumToOrderItem < ActiveRecord::Migration
  def change
    change_table :order_items do |table|
      table.float :sum, null: false, default: 0.0
    end
  end
end
