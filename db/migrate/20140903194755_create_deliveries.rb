class CreateDeliveries < ActiveRecord::Migration
  def change
    create_table :deliveries do |t|
      t.string :name
      t.text :description
      t.attachment :logo

      t.timestamps
    end
  end
end
