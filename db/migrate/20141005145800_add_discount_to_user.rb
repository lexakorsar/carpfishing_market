class AddDiscountToUser < ActiveRecord::Migration
  def change
    change_table :users do |table|
      table.float :discount, null: false, default: 0.0
    end
  end
end
