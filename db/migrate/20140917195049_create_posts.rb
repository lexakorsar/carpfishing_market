class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :description
      t.attachment :title_image
      t.references :admin, index: true

      t.timestamps
    end
  end
end
