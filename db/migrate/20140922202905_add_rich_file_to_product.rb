class AddRichFileToProduct < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.integer :rich_file_id, index: true
    end
  end
end
