class AddHitAndSaleForProduct < ActiveRecord::Migration
  def change
    change_table :products do |table|
      table.boolean :is_hit, default: false
      table.boolean :is_sale, default: false
    end
  end
end
