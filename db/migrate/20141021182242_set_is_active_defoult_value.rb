class SetIsActiveDefoultValue < ActiveRecord::Migration
  def change
    change_column :products, :is_active, :boolean, :default => false
  end
end
