class AddInternalId < ActiveRecord::Migration
  def change
    change_table :products do |table|
      table.string :internal_id
    end
  end
end
