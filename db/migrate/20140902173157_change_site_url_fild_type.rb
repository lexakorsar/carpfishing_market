class ChangeSiteUrlFildType < ActiveRecord::Migration
  def self.up
    remove_column :products, :site_url
    change_table :products do |t|
      t.text :site_url
    end
  end

  def self.down
    remove_column :products, :site_url
    change_table :products do |t|
      t.string :site_url
    end
  end
end
