class AddUserIsGuestField < ActiveRecord::Migration
  def change
    change_table :users do |table|
      table.boolean :is_guest, default: false
    end
  end
end
