class AddIsActiveProduct < ActiveRecord::Migration
  def change
    change_table :products do |table|
      table.boolean 'is_active'
    end
  end
end
