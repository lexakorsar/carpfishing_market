class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :status
      t.string :phone
      t.text :address
      t.string :fio
      t.text :wishes
      t.references :user, index: true

      t.timestamps
    end
  end
end
