class AdLazzyIdToUser < ActiveRecord::Migration
  def change
    change_table(:users) do |t|
      ## Database authenticatable
      t.string :lazy_id
    end
  end
end
