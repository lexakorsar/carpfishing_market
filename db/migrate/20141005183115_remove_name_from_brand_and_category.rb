class RemoveNameFromBrandAndCategory < ActiveRecord::Migration
  def change
    remove_column :categories, :name
    remove_column :brands, :name
  end
end
