class AddUploadConfirmedToOrder < ActiveRecord::Migration
  def change
    change_table :orders do |table|
      table.boolean :upload_confirmed, nil: false, default: false
      table.boolean :uploaded, nil: false, default: false
    end
  end
end
