class AddArticleAndCodeToProduct < ActiveRecord::Migration
  def change
    change_table :products do |table|
      table.string :article
      table.string :product_code

    end
  end
end
