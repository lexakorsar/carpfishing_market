class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :name
      t.text :description
      t.string :type_price
      t.attachment :price

      t.timestamps
    end

    change_table :products do |table|
      table.references :supplier, index: true
    end
  end
end
