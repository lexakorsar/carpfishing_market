class CreateAlbumImages < ActiveRecord::Migration
  def change
    create_table :album_images do |t|
      t.references :album, index: true
      t.attachment :image

      t.timestamps
    end
  end
end
