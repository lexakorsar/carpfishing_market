class AddProductNameToOrderItem < ActiveRecord::Migration
  def change
    change_table :order_items do |table|
      table.string :product_name
    end
  end
end
