# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170421160230) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "album_images", force: :cascade do |t|
    t.integer  "album_id",           limit: 4
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",               limit: 255
  end

  add_index "album_images", ["album_id"], name: "index_album_images_on_album_id", using: :btree

  create_table "albums", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "brands", force: :cascade do |t|
    t.string   "title",                    limit: 255
    t.text     "description",              limit: 65535
    t.string   "title_image_file_name",    limit: 255
    t.string   "title_image_content_type", limit: 255
    t.integer  "title_image_file_size",    limit: 4
    t.datetime "title_image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "internal_id",              limit: 255,   default: "", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "title",                    limit: 255
    t.text     "description",              limit: 65535
    t.integer  "parent_id",                limit: 4
    t.integer  "lft",                      limit: 4
    t.integer  "rgt",                      limit: 4
    t.integer  "depth",                    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title_image_file_name",    limit: 255
    t.string   "title_image_content_type", limit: 255
    t.integer  "title_image_file_size",    limit: 4
    t.datetime "title_image_updated_at"
    t.string   "internal_id",              limit: 255,   default: "",   null: false
    t.integer  "sorting",                  limit: 4
    t.boolean  "is_show",                                default: true
    t.text     "meta_keywords",            limit: 65535
    t.text     "meta_description",         limit: 65535
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",    limit: 255, null: false
    t.string   "data_content_type", limit: 255
    t.integer  "data_file_size",    limit: 4
    t.integer  "assetable_id",      limit: 4
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width",             limit: 4
    t.integer  "height",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0, null: false
    t.integer  "attempts",   limit: 4,     default: 0, null: false
    t.text     "handler",    limit: 65535,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "deliveries", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.text     "description",       limit: 65535
    t.string   "logo_file_name",    limit: 255
    t.string   "logo_content_type", limit: 255
    t.integer  "logo_file_size",    limit: 4
    t.datetime "logo_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "identities", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "provider",   limit: 255
    t.string   "uid",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "news", force: :cascade do |t|
    t.string   "title",                    limit: 255
    t.text     "description",              limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title_image_file_name",    limit: 255
    t.string   "title_image_content_type", limit: 255
    t.integer  "title_image_file_size",    limit: 4
    t.datetime "title_image_updated_at"
  end

  create_table "order_items", force: :cascade do |t|
    t.integer  "product_id",   limit: 4
    t.integer  "order_id",     limit: 4
    t.float    "price",        limit: 24,  default: 0.0, null: false
    t.integer  "count",        limit: 4,   default: 0,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "product_name", limit: 255
    t.float    "sum",          limit: 24,  default: 0.0, null: false
  end

  add_index "order_items", ["order_id"], name: "index_order_items_on_order_id", using: :btree
  add_index "order_items", ["product_id"], name: "index_order_items_on_product_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.string   "status",           limit: 255
    t.string   "phone",            limit: 255
    t.text     "address",          limit: 65535
    t.string   "fio",              limit: 255
    t.text     "wishes",           limit: 65535
    t.integer  "user_id",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",            limit: 255
    t.integer  "delivery_id",      limit: 4
    t.boolean  "upload_confirmed",               default: false
    t.boolean  "uploaded",                       default: false
  end

  add_index "orders", ["delivery_id"], name: "index_orders_on_delivery_id", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.string   "title",                    limit: 255
    t.text     "description",              limit: 65535
    t.string   "title_image_file_name",    limit: 255
    t.string   "title_image_content_type", limit: 255
    t.integer  "title_image_file_size",    limit: 4
    t.datetime "title_image_updated_at"
    t.integer  "admin_id",                 limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "posts", ["admin_id"], name: "index_posts_on_admin_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name",                     limit: 255
    t.float    "price",                    limit: 24
    t.text     "description",              limit: 65535
    t.integer  "category_id",              limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title_image_file_name",    limit: 255
    t.string   "title_image_content_type", limit: 255
    t.integer  "title_image_file_size",    limit: 4
    t.datetime "title_image_updated_at"
    t.integer  "brand_id",                 limit: 4
    t.string   "article",                  limit: 255
    t.string   "product_code",             limit: 255
    t.integer  "supplier_id",              limit: 4
    t.text     "site_url",                 limit: 65535
    t.integer  "rich_file_id",             limit: 4
    t.string   "internal_id",              limit: 255
    t.boolean  "is_active",                              default: false
    t.integer  "hight",                    limit: 4
    t.integer  "width",                    limit: 4
    t.integer  "len_metr",                 limit: 4
    t.integer  "len_fut",                  limit: 4
    t.integer  "count",                    limit: 4
    t.string   "rod_test",                 limit: 255
    t.string   "color",                    limit: 255
    t.string   "flavor",                   limit: 255
    t.boolean  "is_hit",                                 default: false
    t.boolean  "is_sale",                                default: false
    t.string   "original_name",            limit: 255
    t.string   "fake_price",               limit: 255
    t.text     "meta_keywords",            limit: 65535
    t.text     "meta_description",         limit: 65535
    t.float    "currency",                 limit: 24
  end

  add_index "products", ["category_id"], name: "index_products_on_category_id", using: :btree
  add_index "products", ["supplier_id"], name: "index_products_on_supplier_id", using: :btree

  create_table "rich_rich_files", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "rich_file_file_name",    limit: 255
    t.string   "rich_file_content_type", limit: 255
    t.integer  "rich_file_file_size",    limit: 4
    t.datetime "rich_file_updated_at"
    t.string   "owner_type",             limit: 255
    t.integer  "owner_id",               limit: 4
    t.text     "uri_cache",              limit: 65535
    t.string   "simplified_type",        limit: 255,   default: "file"
  end

  create_table "specification_products", force: :cascade do |t|
    t.integer  "product_id",       limit: 4
    t.integer  "specification_id", limit: 4
    t.string   "value",            limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "specification_products", ["product_id"], name: "index_specification_products_on_product_id", using: :btree
  add_index "specification_products", ["specification_id"], name: "index_specification_products_on_specification_id", using: :btree

  create_table "specifications", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "units",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "suppliers", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.text     "description",        limit: 65535
    t.string   "type_price",         limit: 255
    t.string   "price_file_name",    limit: 255
    t.string   "price_content_type", limit: 255
    t.integer  "price_file_size",    limit: 4
    t.datetime "price_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id",        limit: 4
    t.integer  "taggable_id",   limit: 4
    t.string   "taggable_type", limit: 255
    t.integer  "tagger_id",     limit: 4
    t.string   "tagger_type",   limit: 255
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name",           limit: 255
    t.integer "taggings_count", limit: 4,   default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "units", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255,   default: "",    null: false
    t.string   "encrypted_password",     limit: 255,   default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "fio",                    limit: 255
    t.text     "address",                limit: 65535
    t.string   "phone",                  limit: 255
    t.boolean  "guest",                                default: false
    t.string   "lazy_id",                limit: 255
    t.string   "name",                   limit: 255
    t.float    "discount",               limit: 24,    default: 0.0,   null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
